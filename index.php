<?php
/**
 * OpenID/AB 1.0d12 Sample Implementation. 
 *
 * This is a sample implementation of OpenID/AB 1.0d06. 
 * For the informatiion about the spec, go to https://openid4.us/
 * License: GPL v.3
 *
 * @author Nat Sakimura (http://www.sakimura.org/)
 * @version 0.6
 * @create 2010-06-12
**/
include_once("abconstants.php");
include_once('libjsoncrypto.php');
//include_once("libmagicsignatures.php");
include_once("base64url.php");

header('Content-Type: text/html; charset=utf-8');
define('CLIENT_ID', 'https://' . RP_SERVER_NAME . '/abrp/');
define('CLIENT_SECRET', 'shared_secret_is_not_so_secure');
?>
<html>
<head>
<meta name="viewport" content="width=320">
<title>OpenID/AB RP on PHP Sample</title>
</head>
<body>
<?php
$mode = $_GET['mode'];
$code = $_GET['code'];

if(!$mode) {
    if(isset($_GET['code']) && isset($_GET['server_id'])) {
        $mode = 'art_res';
    }
}

if($mode!='art_res'){
?>
<H1>Sample OpenID/AB RP on PHP</h1>
<img src="http://wiki.openid.net/f/openid-logo-wordmark.png" style="float:right;width:30%">

<div id="login">
This is a very simple RP of OpenID Artifact Binding 1.0 draft 06. 
It is made of 2 files: 
<ol>
<li>A request parameter file</li>
<li>This file with simple PHP</li>
</ol>
<p>Entire RP code including this kind of text is only a little over 
200 lines.</p>
<p>This PHP file, when connecting to the OP, is verifying the peer.</p>
<p>
<a href="https://<?php echo OP_SERVER_NAME ?>/abop/op.php?response_type=code&request_uri=https://<?php echo RP_SERVER_NAME ?>/abrp/rfs/rf_ax.json.php&type=web_server&state=blahblahstateblahblah"><img src="http://www.panic.com/jp/coda/img/button-ok.png">Login with <?php echo OP_SERVER_NAME ?> and get Attributes with it. </a>
<font size="-2">
    [<a href="rfs/rf_ax.json.php" target="_blank">Request File (RF)</a>]
</font>
<p>
<a href="https://<?php echo OP_SERVER_NAME ?>/abop/op.php?response_type=code&request_uri=https://<?php echo RP_SERVER_NAME ?>/abrp/rfs/rf_ax_sig.json.php&type=web_server"><img src="http://www.panic.com/jp/coda/img/button-ok.png">Login with <?php echo OP_SERVER_NAME ?> and get Attributes with it, Asym Signed. </a>
<font size="-2">
    [<a href="rfs/rf_ax_sig.json.php" target="_blank">Request File (RF)</a>]
</font>
<p>
<a href="https://<?php echo OP_SERVER_NAME ?>/abop/op.php?response_type=code&request_uri=https://<?php echo RP_SERVER_NAME ?>/abrp/rfs/rf_pape.json.php&type=web_server"><img src="http://www.panic.com/jp/coda/img/button-ok.png">Login with <?php echo OP_SERVER_NAME ?> and get Attributes with it. (PPID)</a>
<font size="-2">
    [<a href="rfs/rf_pape.json.php" target="_blank">Request File (RF)</a>]
</font>

<p>
<a href="https://<?php echo OP_SERVER_NAME ?>/abop/op.php?response_type=code&request_uri=https://<?php echo RP_SERVER_NAME ?>/abrp/rfs/rf_ax_jss.json.php&type=web_server"><img src="http://www.panic.com/jp/coda/img/button-ok.png">Login with <?php echo OP_SERVER_NAME ?> and get Attributes with it, JSON Simple Asym Signed</a>
<font size="-2">
    [<a href="rfs/rf_ax_jss.json.php" target="_blank">Request File (RF)</a>]
</font>

<p>
<a href="https://<?php echo OP_SERVER_NAME ?>/abop/op.php?response_type=code&request_uri=https://<?php echo RP_SERVER_NAME ?>/abrp/rfs/rf_ax_jsenc.json.php&type=web_server"><img src="http://www.panic.com/jp/coda/img/button-ok.png">Login with <?php echo OP_SERVER_NAME ?> and get Attributes with it, JSON Simple Asym Signed and Encrypted</a>
<font size="-2">
    [<a href="rfs/rf_ax_jsenc.json.php" target="_blank">Request File (RF)</a>]
</font>

<p>
<p>
<hr />

<?php
} elseif ($mode == "art_res"){

  $ope = "https://" . OP_SERVER_NAME . "/abop/op.php";

  $url = $ope . "?grant_type=authorization_code&code=" . $code . "&client_id=" . 
   CLIENT_ID . "&client_secret=" . CLIENT_SECRET;
  echo "Since RP received the code, it is making a background 
   request to OP with following URL<p>" ;
  echo "<a href='$url'>$url</a>\n";
  echo "<hr/>";

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $responseText = curl_exec($ch);
  curl_close($ch);
  $oauth_res = json_decode($responseText,true) ;
  if($oauth_res['error']){
	print_r($oauth_res);
	exit;
  }
  $sig_hints = array(array('key_id' => RP_SERVER_NAME, 'secret' => 'aaa'));
  // var_dump($assertion);
  if($oauth_res['type']=='http://openid.net/specs/ab/1.0#signed_format'){
	// Magic Signed Assertion...
	$pubkfname = "certs.pem"; // OP's Certificate File. 
	$pem = file_get_contents($pubkfname);
//	$isValid = magic_verify($responseText,$pem);
	$isValid = json_simple_sign_verify($responseText,$sig_hints);
	if($isValid) {
		$sVal = "<h4>Signature Verification OK</h4>" ;
	} else {
		$sVal = "<h4>Signature Verification FAIL</h4>" ;
	}
	$sar = $oauth_res;
	$oauth_res = json_decode(base64url_decode($sar['data']),true);
  } elseif($oauth_res['type'] == 'http://jsonenc.info/json-encryption/1.0/' && $oauth_res['data_type'] == 'http://openid.net/specs/ab/1.0#openid2json-enc') {
    echo "<p>ENCRYPTED ASSERTION</P>\n";
    $decrypted_obj = js_decrypt($oauth_res, RP_PKEY, true, RP_PKEY_PASSPHRASE);  
    // var_dump($decrypted_obj);
    $signed_json = $decrypted_obj['enc_data'];
    $isValid = json_simple_sign_verify($signed_json,$sig_hints);
    if($isValid) {
        $sVal = "<h4>Signature Verification OK</h4>" ;
    } else {
        $sVal = "<h4>Signature Verification FAIL</h4>" ;
    }
        
    $signed_json_obj = json_decode($signed_json, true);
    // var_dump($signed_json_obj);
    $oauth_res = json_decode(base64url_decode($signed_json_obj['data']), true);
    // var_dump($oauth_res);
    
    $sar = $signed_json_obj;
  }
  $assertion = $oauth_res['openid'];
  $opd="https://" . OP_SERVER_NAME . "/";
  $login=1;
  if($assertion['mode'] != 'id_res') {
      $login=0;
      echo "<br />Error: not id_res: " . $assertion['mode'];
      print_r($assertion);
  }
  if(strpos($assertion['claimed_id'],$opd)!=0){
      $login=0;
      echo "<br />Error: claimed_id mismatch";
  }
  if($assertion['op_endpoint'] != $ope){
     $login=0;
     echo "<br />Error: op_endpoint mismatch:" .$assertion['op_endpoint'];
     echo "!=". $ope;
  }
  if($login==1) {
     // alert("login success");
     $str=beautify_json($responseText);
     if($sar['sig_params']){
	$str .="\n\nDecoding it will give:\n";
	$str .= beautify_json(json_encode($assertion));
     }
     $welcome = "<h1>Welcome " . $assertion["ax.nickname"] 
	. "</h1> You have successfully Logged in!<p>"
	. "<img src='" . $assertion['ax.avatar'] . "'>"
	. "<b>Kanji Name</b>:" . $assertion['ax.lastname#ja_Hani_JP'] 
	. $sVal  
	. "<p>The assertion was as follows<p> <textarea rows='15' cols='40'>"
	. $str . "\n\n" . $pem . "</textarea>"
	. "<br /><a href=\"index.php\">Logout</a>";
	echo $welcome;
   } else {
      echo( "Error: Not completing." );
   }
}

function beautify_json($str) {
	$str=str_replace(',"',',
	"',$str);
	$str=str_replace('{','{	
	',$str);
	$str=str_replace('}','	
}',$str);
	return $str;
}
?>

</body>
</html>
