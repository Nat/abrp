<?php
include_once("../abconstants.php");
include_once('../libjsoncrypto.php');
header('Content-type: text/plain');
$pubkey = base64url_encode(pem2der(file_get_contents(RP_PCERT)));
?>
{
  "type":"http://openid.net/specs/ab/1.0#env",
  "openid":{
    "type":"http://openid.net/specs/ab/1.0#req",
    "immediate":"false",
    "mode":"checkid_setup",
    "claimed_id":"http://specs.openid.net/auth/2.0/identifier_select",
    "identifier":"http://specs.openid.net/auth/2.0/identifier_select",
    "pem_url":"<?php echo RP_PCERT_URL ?>", 
    "enctype":"AES-128-CBC",
    "ns.ax":"http://openid.net/srv/ax/1.1",
    "ax.mode":"fetch_request",
    "ax.avatar":"",
    "ax.nickname":"",
    "ax.email":"",
    "ax.lastname#ja_Hani_JP":"",
    "ax.email":"",
    "ax.lastname#ja_Hani_JP":"", 
    "ax.lastname#ja_Kana_JP":"",
    "ax.firstname#ja_Hani_JP":"",
    "ax.firstname#ja_Kana_JP":"",
    "ax.lastname":"",
    "ax.firstname":"",
    "ax.telephone":"",
    "ax.gender":"",
    "ax.birthYear":"",
    "ax.link":"",
    "ns.pape":"http://specs.openid.net/extensions/pape/1.0",
    "pape.preferred_auth_policies":"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier",
    "pubkey":"<?php echo $pubkey ?>"
  },
  "response_type":"code",
  "client_id":"https://<?php echo RP_SERVER_NAME ?>/abrp/",
  "redirect_uri":"https://<?php echo RP_SERVER_NAME ?>/abrp/index.php",
  "scope":"openid",
  "state":"some_state_information"
}
