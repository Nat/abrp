<?php
include_once("../abconstants.php");
header('Content-type: text/plain');
?>
{
  "type":"http://openid.net/specs/ab/1.0#env",
  "openid":{
    "type":"http://openid.net/specs/ab/1.0#req",
    "immediate":"false",
    "mode":"checkid_setup",
    "claimed_id":"http://specs.openid.net/auth/2.0/identifier_select",
    "identifier":"http://specs.openid.net/auth/2.0/identifier_select",
    "pem_url":"<?php echo RP_PCERT_URL ?>",
    "enctype":"AES-128-CBC",
    "ns.ax":"http://openid.net/srv/ax/1.1",
    "ax.mode":"fetch_request",
    "ax.avatar":"",
    "ax.nickname":"",
    "ax.email":"",
    "ax.lastname#ja_Hani_JP":"", 
    "ax.lastname#ja_Kana_JP":"",
    "ax.firstname#ja_Hani_JP":"",
    "ax.firstname#ja_Kana_JP":"",
    "ax.lastname":"",
    "ax.firstname":"",
    "ax.telephone":"",
    "ax.gender":"",
    "ax.birthYear":"",
    "ax.link":"" 
  },
  "response_type":"code",
  "client_id":"https://<?php echo RP_SERVER_NAME ?>/abrp/",
  "redirect_uri":"https://<?php echo RP_SERVER_NAME ?>/abrp/index.php",
  "scope":"openid",
  "state":"some_state_information"
}