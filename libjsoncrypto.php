<?php

include_once('base64url.php');

/*

    json_simple_sign - signs a json object or string
    
    $data           : json string or array
    $arr_sig_params : array of sig params
                      if algoritm is HMAC-SHA256 =>
                      [
                        { 
                            'algorithm' : 'HMAC-SHA256',                   # required
                            'key_id'    : 'example.com'                    # required
                        }
                        
                      ]
                      
                      if algoritme is RSA-SHA256 =>
                      [
                        { 
                            'algorithm'    : 'RSA-SHA256',                   # optional / default value
                            'certs_uri'    : 'http://example.com/cert.pem'   # required
                            'thumbprint'   : ''                              # optional/ sha1 of DER encoded public certificate
                        }
                        
                      ]
    $arr_keys       : array of secrets and/or array of private key files paths and passwords
                      [ 
                            'aaaa', 
                            'bbbbb',
                            {
                                'key_file' : '/home/www/server.key',
                                'password'   : 'my_key_pass'
                            }

*/

function json_simple_sign($data, $arr_sig_params, $arr_keys, $use_web_token_serialization = 0) {
    $ascii_armoured_payload = base64url_encode(is_array($data) ? json_encode($data) : $data);
    $sigs = array();
    $num_sig_params = count($arr_sig_params);
    for($i = 0; $i < $num_sig_params; $i++) {
        $sig_param = $arr_sig_params[$i];
        $alg = 'RSA-SHA256';
        if(array_key_exists('algorithm', $sig_param)) {
            if(strcasecmp($sig_param['algorithm'], 'HMAC-SHA256') == 0)
                $alg = 'HMAC-SHA256';
            elseif(strcasecmp($sig_param['algorithm'], 'RSA-SHA256') == 0) {
                $alg = 'RSA-SHA256';
                $alg_id = 'sha256';
            }
            elseif(strcasecmp($sig_param['algorithm'], 'RSA-SHA1') == 0) {
                $alg = 'RSA-SHA1';
                $alg_id = OPENSSL_ALGO_SHA1;
            }
        }
        $sig = NULL;
        switch ($alg) {
            case 'RSA-SHA1':
            case 'RSA-SHA256' :
                if(is_array($arr_keys[$i])) {
                    if(isset($arr_keys[$i]['key_file'])) {
                        $fp = fopen($arr_keys[$i]['key_file'], 'r');
                        $priv_key_data = fread($fp, 8192);
                        fclose($fp);
                        
                        $pkey = openssl_pkey_get_private($priv_key_data, $arr_keys[$i]['password']);
                        if($pkey) {
                            /**
                                need to add support for rsa-sha256 algorithm in openssl_sign as last param
                                only implemented in PHP 5.3
                            **/
                            
                            if($alg == 'RSA-SHA256') {
                                digest_sign_data($ascii_armoured_payload, $pkey, $sig);
                            } else {
                                if(openssl_sign($ascii_armoured_payload, $sig, $pkey, $alg_id)) {
                                }
                            }
                            openssl_pkey_free($pkey);
                        }
                    }
                }
                break;
            case 'HMAC-SHA256' :
                $sig = hash_hmac('sha256', $ascii_armoured_payload, $arr_keys[$i], true);
                break;
        }
        $sigs[$i] = base64url_encode($sig);
    }
    
    
                       
    if($use_web_token_serialization) {
        $sig_str = '';
        $num_sigs = count($sigs);
        for($i = 0; $i < $num_sigs; $i++) {
            $sig_str .= ($i ? '.' : '') . $sigs[$i];
        }
        $ascii_armoured_sig_params = base64url_encode(json_encode($arr_sig_params));
        $wts = $sig_str . '.' . $ascii_armoured_payload . '.' . $ascii_armoured_sig_params;
        return $wts;
    }
    
    $signed_obj = array('type' => 'http://openid.net/specs/ab/1.0#signed_format',
                        'data_type' => 'application/json',
                        'data' => $ascii_armoured_payload,
                        'sig_params' => $arr_sig_params,
                        'sigs' => $sigs
                       );
                           
    $signed_json_obj = json_encode($signed_obj);
    return $signed_json_obj;
        
    
    
}


/**
 * Obtain the content of the URL. 
 * @param  String $url      URL from which the content should be obtained. 
 * @return String Response Text. 
 */
function get_url_contents($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $responseText = curl_exec($ch);
    curl_close($ch);
    return $responseText;
}


function json_simple_sign_verify($jss, $sig_hints = NULL) {
    if(is_array($jss))
        $json_obj = $jss;
    elseif(is_string($jss)) {
        if($jss[0] == '{') { // use { as indicator of json encoded string
            $json_obj = json_decode($jss, true);
        }
        else {  // assumes that it's web token serialization
        }
    }
    else return false;
    if(!$json_obj)
        return false;
        
    if(strcmp($json_obj['type'], 'http://openid.net/specs/ab/1.0#signed_format') != 0) {
        return false;
    }
    
    $num_sig_params = count($json_obj['sig_params']);
    $num_sigs = count($json_obj['sigs']);
    if(!$num_sigs) {
        return false;
    }
    
    $num_hints_verified = 0;
    $num_certs_verified = 0;
    $num_hints = $sig_hints ? count($sig_hints) : 0;
    
    if($num_sig_params) {
        for($i = 0; $i < $num_sig_params; $i++) {
            $sig_param = $json_obj['sig_params'][$i];
            if(array_key_exists('certs_uri', $sig_param)) {  // X509 signature value
                if(array_key_exists('algorithm', $sig_param)) {
                    switch($sig_param['algorithm']) {
                        case 'RSA-SHA1':
                            $alg = OPENSSL_ALGO_SHA1;
                            break;
                        case 'RSA-SHA256' :
                            $alg = 'sha256';
                            break;
                        default:
                            return false;
                    }
                } else {
                    $alg = 'sha256';
                }
                    
                $cert = get_url_contents($sig_param['certs_uri']);
                if(!$cert) {
                    echo "unable to get certificate from {$sig_param['certs_uri']}\n";
                    return false;
                } else  {
//                    echo "got cert \n$cert\n\n";
                }
                    
                $pubkeyid = openssl_get_publickey($cert);
                if(!pubkeyid) {
                    echo "Unable to get public key id\n";
                    return false;
                }
                if($alg == 'sha256') {
                    $status = digest_verify_data($json_obj['data'], base64url_decode($json_obj['sigs'][$i]), $pubkeyid);
                } else {
                    $status = openssl_verify($json_obj['data'], base64url_decode($json_obj['sigs'][$i]), $pubkeyid, $alg);
                }
                openssl_free_key($pubkeyid);
                if($status) {
                    ++$num_certs_verified;
                } else {
                    return false;
                }
                
            } elseif (array_key_exists('key_id', $sig_param) && array_key_exists('algorithm', $sig_param)) { // HMAC-256 signature value
                if(strcasecmp($sig_param['algorithm'], 'HMAC-SHA256') != 0) {
                    echo "Unknown signature algorithm {$sig_param['algorithm']}\n\n";
                    continue;
                }
                if(!$sig_hints) {
                    continue;
                }
                for($j = 0; $j < $num_hints; $j++) {
                    if(strcasecmp($sig_param['key_id'], $sig_hints[$j]['key_id']) == 0) {
                        $sig = hash_hmac('sha256', $json_obj['data'], $sig_hints[$j]['secret'], true);
                        if(strcmp(base64url_encode($sig), $json_obj['sigs'][$i]) == 0) {
                            ++$num_hints_verified;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
    }
    
    // at this points, all certificate signatures are verified
    if($num_hints && ($num_hints != $num_hints_verified))  // make sure all hints are verified
        return false;
    if(!$num_certs_verified && !$num_hints_verified) // make sure at least some signatures are verified
        return false;
    return true;
}




function pad_encryption_data($data) {
  // Add a single 0x80 byte and let PHP pad with 0x00 bytes.
  return pack("a*H2", $data, "80");
}
function unpad_encryption_data($data) {
  // Return all but the trailing 0x80 from text that had the 0x00 bytes removed
  return substr(rtrim($data, "\0"), 0, -1);
}

function aes_cbc_encrypt($data, $key, $key_strength, $iv=NULL) {

	$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
	if(!$cipher)
	    return NULL;
	    
	$iv_size = mcrypt_enc_get_iv_size($cipher);
	if(!iv) {
	    $iv = str_repeat(chr(0), 16);  // initialize to 16 byte string of "0"s
	} elseif($iv && $iv_size != strlen($iv)) {
	    echo "Incorrect IV size.\n";
	    return NULL;
	}

  switch ($key_strength) {
    case 128 :
        if(strlen($key) != 16) {
            return NULL;
        }
        break;
    
    case 256:
        if(strlen($key) != 32) {
            return NULL;
        }
        break;
    
    default:
        return NULL;
  }
  
	
	
	
	// This is the plain-text to be encrypted:
	// $cleartext = 'The quick brown fox jumped over the lazy dog';
	$cleartext = pad_encryption_data($data);
		
	// The mcrypt_generic_init function initializes the cipher by specifying both
	// the key and the IV.  The length of the key determines whether we're doing
	// 128-bit, 192-bit, or 256-bit encryption.  
	// Let's do 256-bit encryption here:
	if (mcrypt_generic_init($cipher, $key, $iv) != -1)
	{
		// PHP pads with NULL bytes if $cleartext is not a multiple of the block size..
		$cipherText = mcrypt_generic($cipher,$cleartext );
		mcrypt_generic_deinit($cipher);
		return $cipherText;
	}
    
}


function aes_cbc_decrypt($data, $key, $key_strength, $iv=NULL) {

	$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
	if(!$cipher)
	    return NULL;
	    
	$iv_size = mcrypt_enc_get_iv_size($cipher);
	if(!iv) {
	    $iv = str_repeat(chr(0), $iv_size);  // initialize to 16 byte string of "0"s
	} elseif($iv && $iv_size != strlen($iv)) {
	    echo "Incorrect IV size.\n";
	    return NULL;
	}

  switch ($key_strength) {
    case 128 :
        if(strlen($key) != 16) {
            return NULL;
        }
        break;
    
    case 256:
        if(strlen($key) != 32) {
            return NULL;
        }
        break;
    
    default:
        return NULL;
  }
  
	// This is the plain-text to be encrypted:
	// $cleartext = 'The quick brown fox jumped over the lazy dog';
	$cipherText = $data;
		
	// The mcrypt_generic_init function initializes the cipher by specifying both
	// the key and the IV.  The length of the key determines whether we're doing
	// 128-bit, 192-bit, or 256-bit encryption.  
	// Let's do 256-bit encryption here:
	if (mcrypt_generic_init($cipher, $key, $iv) != -1)
	{
		// PHP pads with NULL bytes if $cleartext is not a multiple of the block size..
		$clearText = mdecrypt_generic($cipher,$cipherText );
		mcrypt_generic_deinit($cipher);
		return unpad_encryption_data($clearText);
	}
    
}


function aes_128_cbc_encrypt($data, $key, $iv = NULL) {
    return aes_cbc_encrypt($data, $key, 128, $iv);
}


function aes_256_cbc_encrypt($data, $key, $iv = NULL) {
    return aes_cbc_encrypt($data, $key, 256, $iv);
}

function aes_128_cbc_decrypt($data, $key, $iv = NULL) {
    return aes_cbc_decrypt($data, $key, 128, $iv);
}


function aes_256_cbc_decrypt($data, $key, $iv = NULL) {
    return aes_cbc_decrypt($data, $key, 256, $iv);
}



function encrypt_with_key($data, $key_file,  $is_private_key=true, $pass_phrase=NULL) {
    if(is_file($key_file)) {
        if(!file_exists($key_file)) return false;
        $handle = fopen($key_file, "r");
        $key_contents = fread($handle, filesize($key_file));
        fclose($handle);
    } else
        $key_contents = $key_file;
    if($is_private_key)
        $key = openssl_pkey_get_private($key_contents, $pass_phrase);
    else 
        $key = openssl_pkey_get_public($key_contents);
    if(!$key) {
        printf("encrypt_with_key -Unable to get %s key\n%s\n", $is_private_key ? 'private' : 'public', $key_file);
        printf("keycontents = %s\n", $key_contents);
        return false;
    }
    $cipherText = NULL;
    if($is_private_key) 
        $status = openssl_private_encrypt($data, $cipherText, $key, OPENSSL_PKCS1_PADDING);
    else
        $status = openssl_public_encrypt($data, $cipherText, $key);
    if(!$status) {
        return false;
    }
    return $cipherText;
}

function decrypt_with_key($data, $key_file, $is_private_key=true, $pass_phrase=NULL) {
    if(is_file($key_file)) {
        if(!file_exists($key_file)) return false;
        $handle = fopen($key_file, "r");
        $key_contents = fread($handle, filesize($key_file));
        fclose($handle);
    } else
        $key_contents = $key_file;
    if($is_private_key)
        $key = openssl_pkey_get_private($key_contents, $pass_phrase);
    else 
        $key = openssl_pkey_get_public($key_contents);
    if(!$key) {
        printf("decrypt_with_key - Unable to get %s key\n%s\n", $is_private_key ? 'private' : 'public', $key_file);
        return false;
    }
    $plainText = NULL;
    if($is_private_key) 
        $status = openssl_private_decrypt($data, $plainText, $key, OPENSSL_PKCS1_PADDING);
    else
        $status = openssl_public_decrypt($data, $plainText, $key);
    if(!$status) {
        return false;
    }
    return $plainText;
}


function js_encrypt($data, $key_file, $is_private_key=false, $pass_phrase=NULL, $public_cert_url=NULL, $enc_key=NULL, $enc_type=NULL) {
    $input_data = gzencode(is_array($data) ? json_encode($data) : $data);
    
    if(is_file($key_file)) {
        if(!file_exists($key_file)) {
            printf("key file %s not found\n", $key_file);
            return false;
        }
    }
    
//    $key_strength = 256;
//    switch($key_strength) {
//        case 128:
//            $key_length = 16;
//            break;
//        case 256:
//            $key_length = 32;
//            break;
//        default:
//            return false;
//    }
    
//    $iv = base64url_encode(openssl_random_pseudo_bytes(16));
//    $key = openssl_random_pseudo_bytes($key_length);
    
    
    $key_strength = 0;
    $key_length = 0;
    if(!($enc_type)) {
        $enc_type = 'AES-128-CBC';
    }
    switch($enc_type) {
        case 'AES-256-CBC':
            $key_strength = 256;
            $key_length = 32;
            break;
        case 'AES-128-CBC':
            $key_strength = 128;
            $key_length = 16;
            break;
        case 'PLAIN':
            break;
        case 'ECDH-ES':
        default:
            return false;
    }
    if($key_strength && $key_length) {
        if($enc_key)
            $key=$enc_key;
        else
            $key = mcrypt_create_iv($key_length, MCRYPT_DEV_URANDOM);
        $encoded_enc_key = base64url_encode(encrypt_with_key($key, $key_file, $is_private_key, $pass_phrase));
        $iv = mcrypt_create_iv(16, MCRYPT_DEV_URANDOM );
        $enc_data = base64url_encode(aes_cbc_encrypt($input_data, $key, $key_strength, $iv));
    } else {  // plain
            $encoded_enc_key = base64url_encode('');
            $iv = '';
            $enc_data = base64url_encode(base64_encode($input_data));
    }

//    if($enc_key)
//        $key=$enc_key;
//    else
//        $key = mcrypt_create_iv($key_length, MCRYPT_DEV_URANDOM);
//    $encoded_enc_key = base64url_encode(encrypt_with_key($key, $key_file, $is_private_key, $pass_phrase));
    
    // $enc_data = base64url_encode(aes_256_cbc_encrypt($input_data, $key, $iv));
    
    $envelope = array(
                        'type'      => 'http://jsonenc.info/json-encryption/1.0/',
                        'data_type' => 'http://openid.net/specs/ab/1.0#openid2json-enc',
                        'enc_key' => $encoded_enc_key,
                        'enc_iv' => base64url_encode($iv),     // 16 byte string, NULL default
                        'enc_data' => $enc_data,
                        'enc_type_asy' => 'http://www.w3.org/2001/04/xmlenc#rsa-1_5', // RSA-v1.5 - http://www.w3.org/2001/04/xmlenc#rsa-1_5 - **default
                                              // RSA-OAEP - http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p
                        'enc_type' => $enc_type, // PLAIN(base64 only), AES-128-CBC(http://www.w3.org/2001/04/xmlenc#aes128-cbc), 
                                          // AES-256-CBC(http://www.w3.org/2001/04/xmlenc#aes256-cbc / **default)
                                          // ECDH-ES(http://www.w3.org/2009/xmlenc11#ECDH-ES)
                        'enc_ref' => $public_cert_url,  // optional url to public pem cert
                        'enc_certs' => '',// optional base64url encoded DER format cert
                        'enc_ephemeral_pub_key ' => '', //(Optional) The ephemeral public key created by the originator 
                                                        //for the use in ECDH-ES encryption. It is a JSON object that has 
                                                        //two parameters "curve" and "key" where "curve" is URI of the 
                                                        //named curve and "key" is the base64url encoded key
                        'enc_thumbprint' => ''// optional base64 encoded SHA1 of the DER encoded certificate
                     );
                     
    return $envelope;                     
    

}


function js_decrypt($data, $key_file, $is_private_key=true, $pass_phrase=NULL) {
    $obj = is_array($data) ? $data : json_decode($data, true) ;

    if(is_file($key_file)) {
        if(!file_exists($key_file)) {
            printf("key file %s not found\n", $key_file);
            return false;
        }
    }
    
    if(strcasecmp($obj['type'], 'http://jsonenc.info/json-encryption/1.0/') != 0) {
        printf("type %s != 'http://jsonenc.info/json-encryption/1.0/'\n", $obj['type']);
        return false;
    }
    if(strcasecmp($obj['data_type'], 'http://openid.net/specs/ab/1.0#openid2json-enc') != 0) {
        printf("data_type %s != 'http://openid.net/specs/ab/1.0#openid2json-enc'\n", $obj['data_type']);
        return false;
    }
    $iv = str_repeat(chr(0), 16);
    if(array_key_exists('enc_iv', $obj))
        $iv = base64url_decode($obj['enc_iv']);
    
    if(!array_key_exists('enc_key', $obj)) {
        printf("enc key doesn't exist\n");
        return false;
    }
    
    if(!array_key_exists('enc_type', $obj)) {
        $obj['enc_type'] = 'AES-128-CBC';
    }
    $key = decrypt_with_key(base64url_decode($obj['enc_key']), $key_file, $is_private_key, $pass_phrase);
    if(!$key && (strcasecmp($obj['enc_type'], 'PLAIN') != 0)) {
        printf("Unable to decrypt enc_key\n");
        return false;
    } else {
        // printf("key = %s length = %d\n", bin2hex($key), strlen(bin2hex($key)));
    }
    
    
    if(!array_key_exists('enc_data', $obj)) {
        printf("enc data doesn't exist\n");
        return false;
    }
    
    $plainText = '';
    $cipherText = base64url_decode($obj['enc_data']);
    switch($obj['enc_type']) {
        case 'PLAIN':
            $plainText = base64_decode($cipherText);
            break;
        case 'AES-256-CBC':
        case 'http://www.w3.org/2001/04/xmlenc#aes256-cbc':
            $plainText = gzdecode(aes_256_cbc_decrypt($cipherText, $key, $iv));
            break;
            
        case 'AES-128-CBC':
        case 'http://www.w3.org/2001/04/xmlenc#aes128-cbc':
            $plainText = gzdecode(aes_128_cbc_decrypt($cipherText, $key, $iv));
            break;
            
        case 'ECDH-ES':
        case 'http://www.w3.org/2009/xmlenc11#ECDH-ES':
            // unsupported by PHP
        default:
            printf("Unsupported enc_type %s\n", $obj['enc_type']);
            return false;
    }
    $obj['enc_data'] = $plainText;
    
    return $obj;
    
}

function gzdecode($data,&$filename='',&$error='',$maxlength=null)
{
    $len = strlen($data);
    if ($len < 18 || strcmp(substr($data,0,2),"\x1f\x8b")) {
        $error = "Not in GZIP format.";
        return null;  // Not GZIP format (See RFC 1952)
    }
    $method = ord(substr($data,2,1));  // Compression method
    $flags  = ord(substr($data,3,1));  // Flags
    if ($flags & 31 != $flags) {
        $error = "Reserved bits not allowed.";
        return null;
    }
    // NOTE: $mtime may be negative (PHP integer limitations)
    $mtime = unpack("V", substr($data,4,4));
    $mtime = $mtime[1];
    $xfl   = substr($data,8,1);
    $os    = substr($data,8,1);
    $headerlen = 10;
    $extralen  = 0;
    $extra     = "";
    if ($flags & 4) {
        // 2-byte length prefixed EXTRA data in header
        if ($len - $headerlen - 2 < 8) {
            return false;  // invalid
        }
        $extralen = unpack("v",substr($data,8,2));
        $extralen = $extralen[1];
        if ($len - $headerlen - 2 - $extralen < 8) {
            return false;  // invalid
        }
        $extra = substr($data,10,$extralen);
        $headerlen += 2 + $extralen;
    }
    $filenamelen = 0;
    $filename = "";
    if ($flags & 8) {
        // C-style string
        if ($len - $headerlen - 1 < 8) {
            return false; // invalid
        }
        $filenamelen = strpos(substr($data,$headerlen),chr(0));
        if ($filenamelen === false || $len - $headerlen - $filenamelen - 1 < 8) {
            return false; // invalid
        }
        $filename = substr($data,$headerlen,$filenamelen);
        $headerlen += $filenamelen + 1;
    }
    $commentlen = 0;
    $comment = "";
    if ($flags & 16) {
        // C-style string COMMENT data in header
        if ($len - $headerlen - 1 < 8) {
            return false;    // invalid
        }
        $commentlen = strpos(substr($data,$headerlen),chr(0));
        if ($commentlen === false || $len - $headerlen - $commentlen - 1 < 8) {
            return false;    // Invalid header format
        }
        $comment = substr($data,$headerlen,$commentlen);
        $headerlen += $commentlen + 1;
    }
    $headercrc = "";
    if ($flags & 2) {
        // 2-bytes (lowest order) of CRC32 on header present
        if ($len - $headerlen - 2 < 8) {
            return false;    // invalid
        }
        $calccrc = crc32(substr($data,0,$headerlen)) & 0xffff;
        $headercrc = unpack("v", substr($data,$headerlen,2));
        $headercrc = $headercrc[1];
        if ($headercrc != $calccrc) {
            $error = "Header checksum failed.";
            return false;    // Bad header CRC
        }
        $headerlen += 2;
    }
    // GZIP FOOTER
    $datacrc = unpack("V",substr($data,-8,4));
    $datacrc = sprintf('%u',$datacrc[1] & 0xFFFFFFFF);
    $isize = unpack("V",substr($data,-4));
    $isize = $isize[1];
    // decompression:
    $bodylen = $len-$headerlen-8;
    if ($bodylen < 1) {
        // IMPLEMENTATION BUG!
        return null;
    }
    $body = substr($data,$headerlen,$bodylen);
    $data = "";
    if ($bodylen > 0) {
        switch ($method) {
        case 8:
            // Currently the only supported compression method:
            $data = gzinflate($body,$maxlength);
            break;
        default:
            $error = "Unknown compression method.";
            return false;
        }
    }  // zero-byte body content is allowed
    // Verifiy CRC32
    $crc   = sprintf("%u",crc32($data));
    $crcOK = $crc == $datacrc;
    $lenOK = $isize == strlen($data);
    if (!$lenOK || !$crcOK) {
        $error = ( $lenOK ? '' : 'Length check FAILED. ') . ( $crcOK ? '' : 'Checksum FAILED.');
        return false;
    }
    return $data;
}


function pem2der($pem_data) {
   $begin = "CERTIFICATE-----";
   $end   = "-----END";
   $pem_data = substr($pem_data, strpos($pem_data, $begin)+strlen($begin));   
   $pem_data = substr($pem_data, 0, strpos($pem_data, $end));
   $der = base64_decode($pem_data);
   return $der;
}

function der2pem($der_data) {
   $pem = chunk_split(base64_encode($der_data), 64, "\n");
   $pem = "-----BEGIN CERTIFICATE-----\n".$pem."-----END CERTIFICATE-----\n";
   return $pem;
}



function digest_sign_data($data, $key, &$signature, $alg='sha256') {

    $sha1_header = pack('H*', '3021300906052b0e03021a05000414');
    $sha256_header = pack('H*', '3031300d060960864801650304020105000420');
    $md5_header = pack('H*', '3020300c06082a864886f70d020505000410');
    $hash = hash($alg, $data, true);
    $sign_data = ${$alg . '_header'};
    if(!$sign_data)
        return false;
    $sign_data .= $hash;

    $cipherText = NULL;
    $status = openssl_private_encrypt($sign_data, $cipherText, $key);
    if(!$status) {
        printf("Unable to encrypt sign data\n");
        return false;
    }
    $signature = $cipherText;
    return true;
}

function digest_verify_data($data, $signature, $key, $alg='sha256') {
    $sha1_header = pack('H*', '3021300906052b0e03021a05000414');
    $sha256_header = pack('H*', '3031300d060960864801650304020105000420');
    $md5_header = pack('H*', '3020300c06082a864886f70d020505000410');
    
    $plainText = NULL;
    $status = openssl_public_decrypt($signature, $plainText, $key);
    if(!$status) {
        printf("Unable to decrypt sign data\n");
        return false;
    }
    
    $hash = hash($alg, $data, true);
    $sign_data .= ${$alg . '_header'};
    if(!$sign_data)
        return false;
    $sign_data .= $hash;
    if($sign_data == $plainText) {
        return true;
    }
    else {
        return false;
    }
}


?>